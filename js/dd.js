/* model */
	function model(){
		if (elems.length>0){
			lvlMax = 1;
			for (var elInd=0; elInd<elems.length; elInd++){
				if (elems[elInd].lvl > lvlMax){
					lvlMax = elems[elInd].lvl;
				}
			}
			for (var lvlInd=1; lvlInd<=lvlMax; lvlInd++){
				
				for (var elInd=0; elInd<elems.length; elInd++){
					if (elems[elInd].lvl = lvlInd){
						elems[elInd].outTest = elems[elInd].out();
						/* подписываем входы и выход*/
						var jn=1;
						for (var inp=0; inp<elems[elInd].inputs; inp++){ 
							$('#'+elems[elInd].id).find('.pinp:eq('+inp+')').text(eval(elems[elInd]['in'+jn]));
							jn++;
							colorSet($('#'+elems[elInd].id).find('.pinp:eq('+inp+')'));
						}
						$('#'+elems[elInd].id).find('.pos').text(elems[elInd].outTest);
						if (elems[elInd].id.slice(0,2)=='tr'){
							$('#'+elems[elInd].id).find('.neg').text(elems[elInd].outTestNeg);
							}
						colorSet($('#'+elems[elInd].id).find('.pos'));
						colorSet($('#'+elems[elInd].id).find('.neg'));
					}
				}
			}
			if (dyn==false){
			$('#infoMess').html('<p>Выходы пересчитаны!</p>');
			$('#infoMess').addClass('AlGreen');
			$('#infoMess').fadeIn('fast');
		  	  			$('#chatAudio')[0].play();
		  	  			setTimeout(function(){
		  	  				$('#infoMess').fadeOut('slow');
		  	  				setTimeout("$('#infoMess').removeClass('AlGreen')",500);
				  	  	}, 10000);
				  	  	
		  	}  			
		}
	}



$(document).ready(function(){
	
	
	var x_end=0;
	var y_end=0;
	var x_dr;
	var y_dr;
	
	
	
	
	$('.size').draggable({
		helper: 'clone',
		opacity: 1
	});
	


	$('#elemField').droppable({
		tolerance: 'fit',
		accept: '.accept',
		drop: function( event, ui ) {
				var drag=ui.draggable;
				var cur=$(drag).data('cur');
				var max=$(drag).data('max');
				var str=$(drag).data('str');
				var Splitstr = str.split(',');
				var now=Splitstr[0];
			    Splitstr.splice(0,1);
				if (Splitstr.length==0){
				$(drag).removeClass('accept');
				}
				$(drag).data('str',Splitstr.join(','));
				
				// Position

			x_end=event.pageX;
			y_end=event.pageY;
			x_dr=x_end-270;
			y_dr=y_end-60;
			var x_mod=x_dr % 20;
			var y_mod=y_dr % 20;
			if (x_mod!=0){
				x_dr-=x_mod;
				
				if (x_dr>270){
					x_dr-=40;
				}
			}
			if (y_mod!=0){
				y_dr-=y_mod
				
				if (y_dr>40){
					y_dr-=40;
				}
			}
			
				
				$(this).append(drag.clone());
				var obj = $(this).find('.fl-elem:last-child');
				$(obj).css({'top':y_dr,'left':x_dr})
				var elementDescr = obj.find('.elDescr');
				elementDescr.append('<p class="idp">'+obj.attr('id')+'_'+now+'</p>')
				obj.attr('id',$(elementDescr).find('.idp').text());
				elementDescr.append('<p class="nump">'+now+'</p>')
				/*   ВХОДЫ И ВЫХОДЫ  */
				var twenty=-20;
				for (var i=1;i<=$(obj).data('ins');i++){
					
				    $(elementDescr).append('<p class="pinp"></p>')
					var top=$(obj).find('.pinp:last-child').css('top');						
					var num=parseFloat(top,10);
					twenty+=20;
					num+=twenty;
					var units=top.slice(-2);
					$(obj).find('.pinp:last-child').css('top',num+units);				
				}
				$(elementDescr).append('<p class="pout pos"></p>')
				$(elementDescr).append('<p class="pout neg"></p>')
  
				
				obj.removeClass('accept')
				   .removeClass('size')
				   .removeClass('ui-draggable')
				   .addClass('fl-elem2')
				   .removeClass('size2');
				
				
				jsPlumb.draggable($('.fl-elem2'),{
					containment: '#elemField',
					grid: [20,20],
				    opacity:0.5
				});
				/*obj.draggable({
					containment: '#elemField',
					grid: [20,20],
				    opacity:0.5
				});
				*/Modalka(obj);
				obj.bind('dblclick',function(event){
					Modalka(this);
				});	
				
				$('#elemField').find('.pinp').html('');
				$('#elemField').find('.pout').html('');
			}
	});
	
	/* ===  кнопка "Вычислить входы" === */
	$('#elemsRun').click(function(){
	    for (var k=0;k<elems.length;k++){ 
			if (dyn){break;}
			for (var j=1;j<=elems[k].inputs;j++){
				if (elems[k]['in'+j].toString().slice(0,2)=='cu'){
					dyn = true;
					break;
				}
				else{
				dyn=false;
				}
			}	
		}   // Dynamic? 
	

		$('.oscUl').html('');
		for (var k=0;k<elems.length;k++){
			//alert(elems[k].id);
			$('.oscUl').append('<li class="select s">'+elems[k].id+'</li>');
//=
	}
		
		if (dyn==false){
			model()
		if (elems.length>0){
			lvlMax = 1;
			for (var elInd=0; elInd<elems.length; elInd++){
				if (elems[elInd].lvl > lvlMax){
					lvlMax = elems[elInd].lvl;
				}
			}
			for (var lvlInd=1; lvlInd<=lvlMax; lvlInd++){
				
				for (var elInd=0; elInd<elems.length; elInd++){
					if (elems[elInd].lvl = lvlInd){
						elems[elInd].outTest = elems[elInd].out();
						/* подписываем входы и выход*/
						var jn=1;
						for (var inp=0; inp<elems[elInd].inputs; inp++){ 
							$('#'+elems[elInd].id).find('.pinp:eq('+inp+')').text(eval(elems[elInd]['in'+jn]));
							jn++;
							colorSet($('#'+elems[elInd].id).find('.pinp:eq('+inp+')'));
						}
						$('#'+elems[elInd].id).find('.pos').text(elems[elInd].outTest);
						if (elems[elInd].id.slice(0,2)=='tr'){
							$('#'+elems[elInd].id).find('.neg').text(elems[elInd].outTestNeg);
							}
						colorSet($('#'+elems[elInd].id).find('.pos'));
						colorSet($('#'+elems[elInd].id).find('.neg'));
					//	$('#'+elems[elInd].id).find('.pout').text(elems[elInd].outTest);
					//	colorSet($('#'+elems[elInd].id).find('.pout'));
					}
				}
			}
			var settime; 
			clearTimeout(settime);
			$('#infoMess').html('<p>Выходы пересчитаны!</p>');
			$('#infoMess').addClass('AlGreen');
			
			
		  	  			$('#chatAudio')[0].play();
		  	  			settime = setTimeout(function(){
		  	  				
		  	  				$('#infoMess').removeClass('AlGreen');
				  	  	}, 10000);
		
			}
		}
		else{
			$('#backgroundNav').addClass('backNav')
			$('#elemsRun i').removeClass('fa-pause')
								.addClass('fa-play');
			if (intId!=undefined){
				clearInterval(intId);
			}
			$('#elemField').find('.pinp').html('');
			$('#elemField').find('.pout').html('');
			$('#dynMod').show();
			
		}
		
	 });
});